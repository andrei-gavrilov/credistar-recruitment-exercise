<?php

use app\models\entities\User;
use app\models\entities\Loan;
use yii\db\Migration;
use yii\i18n\Formatter;

/**
 * Class m191004_182630_json_seed_basic
 */
class m191004_182630_json_seed_basic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $users = json_decode(file_get_contents("users.json"), true);
        
        foreach ($users as $user) {
            $model = new User();
            $model->id = $user['id'];
            $model->first_name = $user['first_name'];
            $model->last_name = $user['last_name'];
            $model->email = $user['email'];
            $model->personal_code = $user['personal_code'];
            $model->phone = $user['phone'];
            $model->active = $user['active'];
            $model->dead = $user['dead'];
            $model->lang = $user['lang'];
            $model->save(false);
        }

        $loans = json_decode(file_get_contents("loans.json"), true);

        foreach ($loans as $loan) {
            $model = new Loan();
            $model->id = $loan['id'];
            $model->user_id = $loan['user_id'];
            $model->amount = $loan['amount'];
            $model->interest = $loan['interest'];
            $model->duration = $loan['duration'];
            
            $model->start_date = Yii::$app->formatter->asDate($loan['start_date']);
            $model->end_date = Yii::$app->formatter->asDate($loan['end_date']);
            $model->campaign = $loan['campaign'];
            $model->status = $loan['status'];
            $model->save(false);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('user');
        $this->truncateTable('loan');
    }
}
