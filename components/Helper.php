<?php
namespace app\components;
use \Datetime;

class Helper
{
    public static function convertPersonalCodeToAge($personal_code) {
        $age=substr($personal_code, 5, 2).'/'.substr($personal_code, 3, 2).'/';
        $temp = substr($personal_code, 0, 1);
        if($temp==3 || $temp==4){
            $age.='19';
        }
        else if($temp==5 || $temp==6){
            $age.='20';
        }
        else if($temp==1 || $temp==2){
            $age.='18';
        }
        $age.=substr($personal_code, 1, 2);
        $date = DateTime::createFromFormat('d/m/Y', $age);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }
    public static function isMajor($personal_code) {
        return (Helper::convertPersonalCodeToAge($personal_code)>=18) ? true : false;
    }
}