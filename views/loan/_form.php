<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\entities\Loan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6"> <?= $form->field($model, 'user_id')->textInput() ?></div>
            <div class="col-md-6"><?= $form->field($model, 'amount')->textInput() ?></div>
        </div>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'interest')->textInput() ?></div>
            <div class="col-md-6"><?= $form->field($model, 'duration')->textInput() ?></div>
        </div>
        <div class="row">
            <div class="col-md-6"> <?= $form->field($model, 'start_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                //'dateFormat' => 'yyyy-MM-dd',
            ]) ?></div>
            <div class="col-md-6"> <?= $form->field($model, 'end_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                //'dateFormat' => 'yyyy-MM-dd',
            ]) ?></div>
        </div>
        <div class="row">
            <div class="col-md-6"><?= $form->field($model, 'campaign')->textInput() ?></div>
            <div class="col-md-6"><?= $form->field($model, 'status')->checkbox() ?></div>
        </div>
    </div>


   

    

    

    

   

    

    

   

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
