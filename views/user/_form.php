<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\entities\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="container">
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'first_name')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'last_name')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'email')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'personal_code')->textInput() ?></div>
    </div>
    <div class="row">
        <div class="col-md-6"><?= $form->field($model, 'phone')->textInput() ?></div>
        <div class="col-md-6"><?= $form->field($model, 'lang')->textInput() ?></div>
    </div>
    <div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'active')->checkbox() ?>
        <?= $form->field($model, 'dead')->checkbox() ?>
    </div>
        
        
    </div>
    </div>
    

    

    

    

    

    

    

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
