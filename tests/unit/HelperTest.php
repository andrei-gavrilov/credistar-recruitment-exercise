<?php
namespace tests\unit;

use app\components\Helper;

class HelperTest extends \Codeception\Test\Unit
{
    public function testPersonalCodeToAgeConversion()
    {
        $this->assertEquals(29 , Helper::convertPersonalCodeToAge(49005025465));
    }
    public function testValidateIfPersonalCodeOwnerIsMajor()
    {
        $this->assertEquals(true , Helper::isMajor(49005025465));
    }
}